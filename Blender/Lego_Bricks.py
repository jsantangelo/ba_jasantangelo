from email.mime import image
import bpy
import random
from mathutils import Euler, Vector
import bmesh
from bpy_extras.object_utils import world_to_camera_view
import mathutils
import json 
import base64


def get_vertex_group_edges(obj):
    if obj.type != 'MESH':
        raise TypeError("The object must be a mesh",obj.name)

    # Ensure that the object has vertex groups
    if not obj.vertex_groups:
        #raise ValueError("The object does not have any vertex groups",obj.name)
        group_verts =  [v.index for v in obj.data.vertices 
                        if obj.data.attributes[0].data[v.index].value ==1]
    
    else:

        # Get the vertices in the vertex group
        group_verts = [v.index for v in obj.data.vertices 
                       if obj.vertex_groups[0].index in [g.group for g in v.groups]]

    # Get the edges that both of their vertices are in the vertex group
    group_edges = [e for e in obj.data.edges if set(e.vertices).issubset(group_verts)]
    used_edges=[]
    sorted_vertices=[]

    #added first edge
    e=group_edges[0]
    sorted_vertices.append(e.vertices[0])
    sorted_vertices.append(e.vertices[1])
    
    for i in range(len(group_edges)-2):
        v , e = edge_find_other_vx(sorted_vertices[-1],group_edges,e)
        sorted_vertices.append(v)


    #sorted_vertices = sort_edge_loop(obj,group_edges[0].vertices[0])
    
    # Convert edges to tuples of vertex world coordinates
    #edges_world_coords = [(obj.matrix_world @ obj.data.vertices[v_idx].co) for e in group_edges for v_idx in e.vertices]
    edges_world_coords = [(obj.matrix_world @ obj.data.vertices[v].co) for v in sorted_vertices]

    return edges_world_coords




def edge_find_other_vx(v,edge_list,current_e):
    for e in edge_list:
        if(e != current_e and v in set(e.vertices)):
            if(e.vertices[0]==v):
                return e.vertices[1],e
            else:
                return e.vertices[0],e


def calculate_world_bbox(obj):
    # Get bounding box corners in local space
    local_bbox_corners = [Vector(corner) for corner in obj.bound_box]#corners are connected vertices

    # Transform to world space
    world_bbox_corners = [obj.matrix_world @ corner for corner in local_bbox_corners]

    # Calculate min and max points
    min_point = Vector((min(corner.x for corner in world_bbox_corners),
                        min(corner.y for corner in world_bbox_corners),
                        min(corner.z for corner in world_bbox_corners)))

    max_point = Vector((max(corner.x for corner in world_bbox_corners),
                        max(corner.y for corner in world_bbox_corners),
                        max(corner.z for corner in world_bbox_corners)))

    return min_point, max_point

def check_collision(obj1, obj2):
    # Apply the object's matrix_world to its bounding box points to get them in world coordinates
    bounds1 = [obj1.matrix_world @ mathutils.Vector(corner) for corner in obj1.bound_box]
    bounds2 = [obj2.matrix_world @ mathutils.Vector(corner) for corner in obj2.bound_box]

    # Calculate the min and max bounds in all dimensions for obj1
    min1_x = min(point.x for point in bounds1)
    min1_y = min(point.y for point in bounds1)
    min1_z = min(point.z for point in bounds1)
    max1_x = max(point.x for point in bounds1)
    max1_y = max(point.y for point in bounds1)
    max1_z = max(point.z for point in bounds1)

    # Calculate the min and max bounds in all dimensions for obj2
    min2_x = min(point.x for point in bounds2)
    min2_y = min(point.y for point in bounds2)
    min2_z = min(point.z for point in bounds2)
    max2_x = max(point.x for point in bounds2)
    max2_y = max(point.y for point in bounds2)
    max2_z = max(point.z for point in bounds2)

    # Check for a collision by comparing the bounds
    collision_x = min1_x <= max2_x and max1_x >= min2_x
    collision_y = min1_y <= max2_y and max1_y >= min2_y
    collision_z = min1_z <= max2_z and max1_z >= min2_z

    return collision_x and collision_y and collision_z

def bmesh_copy_from_object(obj, transform=True, triangulate=True, apply_modifiers=False):
    """
    Returns a transformed, triangulated copy of the mesh
    """

    assert(obj.type == 'MESH')

    if apply_modifiers and obj.modifiers:
        me = obj.to_mesh(bpy.context.scene, True, 'PREVIEW', calc_tessface=False)
        bm = bmesh.new()
        bm.from_mesh(me)
        bpy.data.meshes.remove(me)
    else:
        me = obj.data
        if obj.mode == 'EDIT':
            bm_orig = bmesh.from_edit_mesh(me)
            bm = bm_orig.copy()
        else:
            bm = bmesh.new()
            bm.from_mesh(me)

    # Remove custom data layers to save memory
    for elem in (bm.faces, bm.edges, bm.verts, bm.loops):
        for layers_name in dir(elem.layers):
            if not layers_name.startswith("_"):
                layers = getattr(elem.layers, layers_name)
                for layer_name, layer in layers.items():
                    layers.remove(layer)

    if transform:
        bm.transform(obj.matrix_world)

    if triangulate:
        bmesh.ops.triangulate(bm, faces=bm.faces)

    return bm

def bmesh_check_intersect_objects(obj, obj2):#Take 2 objects and checks if they intersect or not
    """
    Check if any faces intersect with the other object

    returns a boolean
    """
    assert(obj != obj2)#Incase they are the same objects -> error

    bm = bmesh_copy_from_object(obj, transform=True, triangulate=True)#convert blender object to bmesh object
    bm2 = bmesh_copy_from_object(obj2, transform=True, triangulate=True)
    #added alter
    ch=bmesh.ops.convex_hull(bm, input=bm.verts)
    ch2=bmesh.ops.convex_hull(bm2, input=bm2.verts)
    
    for v in ch['geom']:
        v.select = True

    for v in ch2['geom']:
        v.select = True


    # If bm has more edges, use bm2 instead for looping over its edges
    # (casts less rays from the simpler object to the more complex object)
    if len(bm.edges) > len(bm2.edges):#optimization purposes
        bm2, bm = bm, bm2

    # Create a real mesh (lame!)
    scene = bpy.context.scene
    me_tmp = bpy.data.meshes.new(name="~temp~")#creates temporary object
    bm2.to_mesh(me_tmp)#saves mesh 2 to temporary object to not change anything in original object
    bm2.free()#frees mesh 2
    obj_tmp = bpy.data.objects.new(name=me_tmp.name, object_data=me_tmp)#assigns an object to temp mesh
    bpy.context.collection.objects.link(obj_tmp)
    ray_cast = obj_tmp.ray_cast

    intersect = False

    EPS_NORMAL = 0.000001
    EPS_CENTER = 0.01  # should always be bigger

    #for ed in me_tmp.edges:
    for ed in bm.edges:#iterates through edges of mesh 1
        v1, v2 = ed.verts#each edge has 2 verticies

        # setup the edge with an offset
        co_1 = v1.co.copy()#security purposes (dont want to affect the actual mesh)
        co_2 = v2.co.copy()
        co_mid = (co_1 + co_2) * 0.5#calculates mid point of each edge
        no_mid = (v1.normal + v2.normal).normalized() * EPS_NORMAL#calculates normal of mid edge
        co_1 = co_1.lerp(co_mid, EPS_CENTER) + no_mid
        co_2 = co_2.lerp(co_mid, EPS_CENTER) + no_mid
        #no is normal
        success, co, no, index = ray_cast(co_1, (co_2 - co_1).normalized(), distance = ed.calc_length())#doing the raycast 
        if index != -1:#-1 if collision happens
            intersect = True
            break

    # scene.objects.unlink(obj_tmp)
    bpy.context.collection.objects.unlink(obj_tmp)#unlinks temporary mesh, not needed after raycast
    bpy.data.objects.remove(obj_tmp)#removes object
    bpy.data.meshes.remove(me_tmp)#removes data


    return intersect


#starting here...
bricks = list(bpy.data.collections["bricks"].all_objects)#list of brick objects in brick collection
print("bricks count: ",len(bricks))#checks how many bricks we have

cam = bpy.context.scene.camera#finds active camera and returns it
scene = bpy.context.scene#returns current scene
render_scale = scene.render.resolution_percentage / 100
render_size = Vector((scene.render.resolution_x * render_scale,
                      scene.render.resolution_y * render_scale))#resolution x and y
border_obj=bpy.data.objects["border"]#screen border intersection check
aspect_ratio=scene.render.resolution_y/scene.render.resolution_x #9/16

for frame in range (1,10):#set to render 9 frames
    #select random amount of bricks
    
    bricks_in_use = random.sample(list(bricks), 17)#select random bricks


    #place them on table
    max_X=0.12 #border of camera sight X
    max_Y=0.22 #border of camera sight Y
    rotations = [0,1.5708,3.14259,4.71239]#0 90 180 279 degrees 
    for brick in bricks:
        brick.location=(0,0,-1)#move all bricks to hidden place

    for brick in bricks_in_use:#only the bricks chosen above
        rand_X=random.random()*max_X*2 - max_X #between -0.12 and 0.12
        rand_Y=random.random()*max_Y*2 - max_Y#between +0.22 and -0.22
        rand_rot=random.choice(rotations)#0 90 180 279 degrees 
        #rand_rot=random.random()*2*3.14259 #enable this to return to random local z axis rotations (not 0 90 180 270)
        brick.location = (rand_X,rand_Y,0)#setting object location for X and Y
        brick.rotation_euler = [0,0,rand_rot]#only Z is affected in rotation


    #detect intersecting ones 
    intersecting_bricks=[]#puts intersecting bricks in an array
    for br1_id,brick_1 in enumerate(bricks_in_use):
        
        if(bmesh_check_intersect_objects(brick_1,border_obj)):#it intersects with border object
            brick_1.location=(0,0,-1)#hides the object
            print(brick_1.name, " - border intersected removing")
            bricks_in_use.remove(brick_1)#removes hidden object
            continue

        for brick_2 in bricks_in_use:#brick_1 not in intersecting_bricks and
            if( brick_1 != brick_2 and bmesh_check_intersect_objects(brick_1,brick_2)):#check intersection and if obj1 and 2 are the same
                brick_1.location=(0,0,-1)#guides the object
                print(brick_1.name, " - intersected removing")
                bricks_in_use.pop(br1_id)#remove brick from usable bricks array

    

    #calculate x y width height 
    file_content=""#create empty file content for yolo data
    for brick in bricks_in_use:#iterate through usable bricks 
        co_2d = world_to_camera_view(scene, cam, brick.location)#calculate screen position of objects
        min_point,max_point=calculate_world_bbox(brick)#calculate bounding box points
        dim_multiplier=3.6
        min_point*=dim_multiplier
        max_point*=dim_multiplier
        width,height=(max_point.x-min_point.x),(max_point.y-min_point.y)

        #debug
        print(brick.name, " - pixels: " , co_2d," - Width/height= ",)

        #create yolo label file
        render_dir = bpy.context.scene.render.filepath 
        file_content+= brick.name+" "+str(co_2d.x)+" "+str(1-co_2d.y)+" "+str(height*0.75)+" "+str(width)+"\n"
    f= open(render_dir+str(frame).zfill(4)+".txt","w+")
    f.write(file_content)
    f.close()


    #render and save  image 
    bpy.context.scene.frame_start = frame
    bpy.context.scene.frame_end = frame
    
    bpy.data.scenes["Scene"].node_tree.nodes["Value"].outputs[0].default_value=0.0#normal render mode
    bpy.data.scenes["Scene"].render.filter_size=1.5
    bpy.ops.render.render(animation=True, use_viewport=True)    
    
    bpy.data.scenes["Scene"].node_tree.nodes["Value"].outputs[0].default_value=1.0#AOV mode
    
    bpy.context.scene.render.filepath += "seg_"
    bpy.data.scenes["Scene"].render.filter_size=0.01
    bpy.ops.render.render(animation=True, use_viewport=True)    
    
    bpy.context.scene.render.filepath = bpy.context.scene.render.filepath[:-4]

    #loading last rendered image to put into json
    with open(bpy.context.scene.render.filepath+str(str(frame).zfill(4))+".jpg",mode="rb") as file:
        image=file.read()
        img_data = base64.encodebytes(image).decode('utf-8')

    poly_anonation={"version": "5.2.1","flags":{},"shapes": [], "imagePath": str(str(frame).zfill(4)+'.jpg'),"imageData":base64.b64encode(image) }
    #image data going into json
    poly_anonation["imageData"]=img_data
    
    #polygon anonations calculation
    dg = bpy.context.evaluated_depsgraph_get()
    for brick in bricks_in_use:
        brick = brick.evaluated_get(dg)

        anonation_verticies = get_vertex_group_edges(brick)
        print("ann verticies: ",brick.name)
        pixel_coords=[]
        for v in anonation_verticies:
            cam_0_1=(world_to_camera_view(scene, cam, v))[:-1]
            x=cam_0_1[0]
            y=1-cam_0_1[1]
            x*=render_size.x
            y*=render_size.y
            pixel_coords.append([x,y])
            
        poly_anonation["shapes"].append({"label":brick.name,"points":pixel_coords})
    

    #extract poly anonation to JSON
    with open(bpy.context.scene.render.filepath +"poly_"+str(frame).zfill(4)+".json", "w") as outfile:
            json.dump(poly_anonation, outfile)#file write
            outfile.close()

