# Preface
This GitHub repository contains the software for building a pipeline for synthetic data generation and object detection.

## Requirements
The required libraries and packages are listed in yolov5/requirements.txt.

# Blender
The Blender folder contains the files for creating synthetic images.
This includes a folder with the random backgrounds used in the generated images, a folder with the .stl files of the lego bricks, the blender files and the script written for generating the images.

The project uses Blender 3.4, please click on [Blender 3.4](https://www.blender.org/download/releases/3-4/) to download or update.


### Generating Synthetic Images:

To generate the synthetic images open the "bricks_V3.blend" file and go to the scripting tab at the top:

![Scripting](YOLOV5/Read_Me_Images/Scripting.png)

Then click the folder icon and select the "Lego_bricks.py" file.

![Lego_Bricks_File](YOLOV5/Read_Me_Images/Lego_Bricks_File.png)

Edit line 224 of the script to set the number of images to render.

![Number_of_renders](YOLOV5/Read_Me_Images/Number_of_renders.png)

Edit line 227 of the script to set the maximum number of bricks that should appear in a rendered image.

![Number_of_bricks](YOLOV5/Read_Me_Images/Number_of_bricks.png)

Make sure to select the location for the rendered images and lables to be saved by clicking the folder icon in the image below. 

![Save_Location](YOLOV5/Read_Me_Images/Save_Location.png)
  
The script outputs labels for each image as text files, which are saved in the same folder as the images. The label files have the same file name as the corresponding image files. These text files are formatted so that the [yolov5 neural network](https://github.com/ultralytics/yolov5) can be trained on them. A text file contains the class of the object, the x and y coordinates of the center, the width and the height of the bounding box.


# YOLOv5

## 1- First "Clone the YOLOv5 Github Repository" and "Install Requirements" to make sure that the required libraries and dependencies are installed.

## 2- Prepare training images with labels and prepare a .yaml file for training. Make sure to move the .yaml file to the same folder where the cloned YOLOv5 repo is.

## 3- To train the model, run the sample code below "training model". Use "train.py" to train the model and define the Hyperparameters preferred (ex. number of epochs, image size...)

## 4- After training, validate the YOLOv5 model by running the sample code below "validate model". Use "val.py" to validate the model.

## 5- To test the model on a new dataset use "val.py". See the test code below.

## 6- To use the trained YOLOv5 model weights on a single image or a number of images, use "detect.py". See the code below.

## Note: Results for training, validations and detection are saved in the "runs" folder.


### Clone the YOLOv5 repo
!git clone https://github.com/ultralytics/yolov5
%cd yolov5
!pip install -r requirements.txt


### Train YOLOv5
# Use train.py to train the model
!python train.py --img 640 --batch 16 --epochs 100 --data training_data.yaml --weights yolov5s.pt --device 0


### Validate YOLOv5 model
!python val.py --img 640 --data dataset.yaml --weights 15_best.pt --device 0 


### Test YOLOv5 model on a new dataset
!python val.py --img 640 --data new_dataset.yaml --weights 15_best.pt --task test --device 0


### Use the model for detections
!python detect.py --weights 15_best.pt --source path_to_images --device 0




# YOLOv5 Inference Script

## Overview

The Python script "detect.py" performs object detection inference using YOLOv5 on various sources such as images, videos, directories, webcams, and more.

## Requirements

Before using this script, ensure that you have the following dependencies installed:

- Python (>=3.6)
- PyTorch
- OpenCV
- NumPy
- Other required libraries (install using `pip install -r requirements.txt`)

## Usage

You can run the inference script using the following command:
!python detect.py --weights yolov5s.pt --source input.jpg

## Source Types
--source 0: Webcam
--source 1: USB Camera
--source img.jpg: Single image
--source vid.mp4: Video file
--source screen: Screenshot
--source path/: Directory containing images
--source list.txt: List of image paths


## Detection information
The following lines of code can be edited to change the position, color, font style, font size etc. of the detection information.
The detection information displays the brick type, its coordinates and the confidence of the detection.

![Detection_information](YOLOV5/Read_Me_Images/Detection_information.png)